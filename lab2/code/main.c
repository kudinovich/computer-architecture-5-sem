#include <LPC23xx.H>

#define BIT_BTTN (1 << 29)

#define STB 26 //Port1.26
#define CLK 27 //Port1.27
#define DIO 28 //Port1.28

void delay(unsigned int count)
{
	unsigned int i;
	
	for (i = 0; i < count; i++) {}
}

void tm1638_sendbyte(unsigned int x)
{	
	unsigned int i;
	
	IODIR1 |= (1 << DIO); // Устанавливаем пин DIO на вывод
	
	for (i = 0; i < 8; i++)
	{
		IOCLR1 = (1 << CLK); // Сигнал CLK устанавливаем в 0
		delay(0xFFF);
		
		if (x & 1)
			IOSET1=(1 << DIO); // Устанавливаем значение на выходе DIO
		else
			IOCLR1 = (1 << DIO);
		delay(0xFFF);
		
		x >>= 1;
		IOSET1 = (1 << CLK); // Сигнал CLK устанавливаем в 1
		delay(0x1FFF);
	}
}

unsigned int tm1638_receivebyte()
{
	unsigned int i;
	unsigned int x = 0;
	
	IODIR1 &= ~(1 << DIO); // Устанавливаем пин DIO на ввод
	
	for (i = 0; i < 32; i++)
	{
		IOCLR1 = (1 << CLK); // Сигнал CLK устанавливаем в 0
		delay(0xFFF);
		
		if (IOPIN1 & (1 << DIO))
			x |= (1 << i);
		delay(0xFFF);
		
		IOSET1 = (1 << CLK); // Сигнал CLK устанавливаем в 1
		delay(0x1FFF);			
	}
	
	return x;
}

void tm1638_sendcmd(unsigned int x)
{
	// Устанавливаем пассивный высокий уровень сигнала STB
	IOSET1 = (1 << STB);
	
	// Устанавливаем пины CLK,DIO,STB на вывод
	IODIR1 = (1 << CLK) | (1 << DIO) | (1 << STB);
	
	// Устанавливаем активный низкий уровень сигнала STB
	IOCLR1 = (1 << STB);
	tm1638_sendbyte(x);
}


void tm1638_setadr(unsigned int adr)
{
	// Установить адрес регистра LED инидикации
	tm1638_sendcmd(0xC0 | adr);	
}

void tm1638_init()
{
	unsigned int i;
	
	// Разрешить работу индикации
	tm1638_sendcmd(0x88);
	
	// Установить режим адресации: автоинкремент
	tm1638_sendcmd(0x40);
	
	// Установить адрес регистра LED инидикации
	tm1638_setadr(0);
	
	// Сбросить все 
	for (i = 0; i <= 0xF; i++)
		tm1638_sendbyte(0);
	
	// Установить режим адресации: фиксированный
	tm1638_sendcmd(0x44);
}

/*// Гасим диоды
for (i = 1; i < 6; i += 2)
{
	tm1638_setadr(i); // Устанавливаем адрес
	tm1638_sendbyte(0); // Шлём данные
}
delay(0xffff);*/

int main (void)
{
	unsigned int i;
	tm1638_init();
	
	while (1)
	{
		i = 1;
		
		// Проверяем нажатие кнопки
		tm1638_sendcmd(0x46); // 0100 0110
		i = tm1638_receivebyte();
		
		if (i == 1) // 0 - вкл, 1 - выкл
		{
			// Прекращаем поворот по X и съёмку
			tm1638_setadr(1); // Устанавливаем адрес
			tm1638_sendbyte(0); // Гасим диоды
			tm1638_setadr(3);
			tm1638_sendbyte(0);
			
			// Начинаем поворот по У
			tm1638_setadr(5); // Устанавливаем адрес
			tm1638_sendbyte(1); // Шлём данные
		}
		else
		{
			// Прекращаем поворот по Y
			tm1638_setadr(5);
			tm1638_sendbyte(0);
			
			// Начинаем поворот по Х и съёмку
			tm1638_setadr(1);
			tm1638_sendbyte(1);
			tm1638_setadr(3);
			tm1638_sendbyte(1);
		}
		delay(0xFFFFF);
	}	
}
